Purple (Pidgin) plugin for new mail notifications.
==================================================

Pidgin has a built-in feature for new mail notifications.
Unfortunately, this feature depends on the service provider.
If you are affected by this issue, then this plugin is for you.

Dependencies
------------

This plugin depends on libpurple (Pidgin) and libetpan.

Install
-------

There is a PKGBUILD provided for ArchLinux.
In general, use `make` to build and `make install` to install the plugin
system-wide or `make install_home` to install in your local pidgin directory.

Usage
-----

Enable IMAP access to your mail account.
Once installed start (or re-start) pidgin. The plugin should appear in
the Plugins window `Ctrl-U`. Enable and configure it.
In the Accounts window `Ctrl-A` make sure that `New mail notifications`
is checked for each account wanted.
Sign-in by changing your status from `Offline`.

License
-------

This software is licensed under the 0BSD license (BSD Zero Clause License).

https://spdx.org/licenses/0BSD.html

Caveats
-------

Only tested with gmail.
Only tested with a single account.

If you received an instant message while offline
you will not see it in Pidgin when you log back in.
The folder to be checked can be set to `[Gmail]/All Mail`
instead of `INBOX`, but that does not solve the issue.

If you do not mark the mails as read between checks, you will continue
getting notifications for already notified mails.
