// SPDX-License-Identifier: 0BSD

#define PURPLE_PLUGINS

#include <conversation.h>
#include <debug.h>
#include <glib.h>
#include <libetpan.h>
#include <version.h>

#define PLUGIN_NAME "libetpan mail check"

enum {CHECKING_PERIOD = 8};

#define PREFS_BASE "/plugins/core/" PLUGIN_NAME
static const char *CHECK_PERIOD = PREFS_BASE "/check_period";

struct mail {
  PurpleConnection   *conn;
  struct mailstorage *storage;
  struct mailfolder  *folder;
  unsigned            errors;
};

struct plugin {
  PurplePlugin *plugin;
  struct mail  *data; // array, one item per account
  unsigned      length; // length of data
  guint         timer;
};

static void check_mail(struct mail *data) {
  PurpleConnection   *conn;
  PurpleAccount      *account;
  char const         *account_name;
  struct mailstorage *storage;
  struct mailfolder  *folder;
  uint32_t            messages;
  uint32_t            recent;
  uint32_t            unseen;
  int                 i;

  conn         = data->conn;
  folder       = data->folder;
  storage      = data->storage;
  account      = purple_connection_get_account(conn);
  account_name = purple_account_get_name_for_display(account);

  i = mailfolder_connect(folder);
  if (i != MAIL_NO_ERROR) {
    purple_debug_error(PLUGIN_NAME, "%s: mailfolder_connect %d\n", account_name, i);
    goto error;
  }

  i = mailfolder_status(folder, &messages, &recent, &unseen);
  mailfolder_disconnect(folder);
  mailstorage_disconnect(storage);
  if (i != MAIL_NO_ERROR) {
    purple_debug_error(PLUGIN_NAME, "%s: mailfolder_status %d\n", account_name, i);
    goto error;
  }

  purple_debug_info(PLUGIN_NAME, "%s: messages=%d, recent=%d, unseen=%d\n", account_name, messages, recent, unseen);
  if (unseen) {
    purple_notify_email(conn, "There is unread mail.", folder->fld_pathname, account_name, "", NULL, NULL);
  }

  data->errors = 0;
  return;

error:
  ++data->errors;
  if (data->errors > 2) {
    // Only report errors after 3 consecutive failed attempts.
    data->errors = 0;
    purple_notify_email(
      conn, "Error: mail check failed for this account.", folder->fld_pathname, account_name, "", NULL, NULL
    );
  }
  return;
}

static gboolean check_mails(struct plugin *plug) {
  for (unsigned u = 0; u < plug->length; ++u) {
    if (plug->data[u].conn) {
      check_mail(&plug->data[u]);
    }
  }
  return TRUE;
}

static void signed_on(PurpleConnection *conn, PurplePlugin *plugin) {
  char           user[128];
  char           serv[128];
  struct mail   *data;
  struct plugin *plug;
  PurpleAccount *account;
  char const    *account_name;
  char const    *username;
  char          *at;
  unsigned       u;
  int            i;

  plug         = plugin->extra;
  account      = purple_connection_get_account(conn);
  account_name = purple_account_get_name_for_display(account);

  if (!purple_account_get_check_mail(account)) {
    // "New mail notifications" check box is un-checked.
    return;
  }

  // Get full user name: john.doe@example.com/
  username = purple_account_get_username(account);
  at       = strchr(username, '@');

  // Get just the user part: john.doe
  i = snprintf(user, sizeof(user), "%.*s", (int)(at - username), username);
  if (!at || i < 1 || i >= sizeof(user)) {
    purple_debug_error(PLUGIN_NAME, "%s: username\n", account_name);
    goto error0;
  }

  // Get the server part prepended with "imap.": imap.example.com
  i = snprintf(serv, sizeof(serv), "imap.%s", at + 1);
  if (i < 8 || i >= sizeof(serv)) {
    purple_debug_error(PLUGIN_NAME, "%s: servname\n", account_name);
    goto error0;
  }
  for (; serv[i - 1] == '/'; --i);
  serv[i] = 0;

  // Get an empty slot in the data array.
  for (u = 0; u < plug->length && plug->data[u].conn; ++u);
  if (u == plug->length) {
    struct mail *tmp = realloc(plug->data, (u + 1) * sizeof(*tmp));
    if (!tmp) {
      purple_debug_error(PLUGIN_NAME, "%s: realloc\n", account_name);
      goto error0;
    }
    plug->data = tmp;
    ++plug->length;
  }
  data = &plug->data[u];
  data->conn = conn;

  // libetpan setup
  data->storage = mailstorage_new(NULL);
  if (!data->storage) {
    purple_debug_error(PLUGIN_NAME, "%s: mailstorage_new\n", account_name);
    goto error1;
  }

  i = imap_mailstorage_init(
    data->storage,
    serv,
    993,
    NULL,
    CONNECTION_TYPE_TLS,
    IMAP_AUTH_TYPE_PLAIN,
    user,
    purple_account_get_password(account),
    0,
    NULL
  );
  if (i != MAIL_NO_ERROR) {
    purple_debug_error(PLUGIN_NAME, "%s: imap_mailstorage_init %d\n", account_name, i);
    goto error2;
  }

  data->folder = mailfolder_new(data->storage, "INBOX", NULL);
  if (!data->folder) {
    purple_debug_error(PLUGIN_NAME, "%s: mailfolder_new\n", account_name);
    goto error2;
  }

  // Do a first check when signing-in.
  return check_mail(data);

error2:
  mailstorage_free(data->storage);
error1:
  *data = (struct mail){};
error0:
  purple_notify_email(conn, "Error: mail check disabled for this account.", "", account_name, "", NULL, NULL);
}

static void signed_off(PurpleConnection *conn, PurplePlugin *plugin) {
  struct plugin      *plug;
  PurpleAccount      *account;
  char const         *account_name;
  struct mailstorage *storage;
  struct mailfolder  *folder;
  unsigned            u;

  plug = plugin->extra;

  // Get the associated slot from the data array.
  for (u = 0; u < plug->length && plug->data[u].conn != conn; ++u);
  if (u == plug->length) {
    // Signed-off from an unmanaged account.
    return;
  }

  storage       = plug->data[u].storage;
  folder        = plug->data[u].folder;
  plug->data[u] = (struct mail){};

  mailfolder_free(folder);
  mailstorage_free(storage);

  account      = purple_connection_get_account(conn);
  account_name = purple_account_get_name_for_display(account);
  purple_debug_info(PLUGIN_NAME, "Stopped mail checker for %s.\n", account_name);
}

static void new_check_period(const char *name, PurplePrefType type, gconstpointer val, gpointer data) {
  struct plugin *plug = data;
  int check_period = GPOINTER_TO_INT(val);
  if (check_period > 0) {
    if (plug->timer) {
      purple_timeout_remove(plug->timer);
    }
    plug->timer = purple_timeout_add_seconds(check_period * 60, (GSourceFunc)check_mails, plug);
    purple_debug_info(PLUGIN_NAME, "Checking every %d minutes\n", check_period);
  }
}

static gboolean plugin_load(PurplePlugin *plugin) {
  struct plugin *plug = malloc(sizeof(*plug));
  if (!plug) {
    purple_debug_error(PLUGIN_NAME, "malloc\n");
    return FALSE;
  }
  *plug = (struct plugin){.plugin = plugin};
  plugin->extra = plug;

  new_check_period(NULL, 0, GINT_TO_POINTER(CHECKING_PERIOD), plug);

  purple_prefs_add_none(PREFS_BASE);
  purple_prefs_add_int(CHECK_PERIOD, CHECKING_PERIOD);
  purple_prefs_connect_callback(purple_prefs_get_handle(), CHECK_PERIOD, new_check_period, plug);
  purple_signal_connect(purple_connections_get_handle(), "signed-on", plugin, PURPLE_CALLBACK(signed_on), plugin);
  purple_signal_connect(purple_connections_get_handle(), "signed-off", plugin, PURPLE_CALLBACK(signed_off), plugin);
  return TRUE;
}

static gboolean plugin_unload(PurplePlugin *plugin) {
  struct plugin *plug = plugin->extra;

  purple_timeout_remove(plug->timer);

  for (unsigned u = 0; u < plug->length; ++u) {
    if (plug->data[u].conn) {
      signed_off(plug->data[u].conn, plugin);
    }
  }

  plugin->extra = NULL;
  free(plug->data);
  *plug = (struct plugin){};
  free(plug);

  return TRUE;
}

static void init_plugin(PurplePlugin *plugin) {}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin) {
  PurplePluginPrefFrame *frame = purple_plugin_pref_frame_new();
  PurplePluginPref *ppref;

  ppref = purple_plugin_pref_new_with_name_and_label(CHECK_PERIOD, "Time between checks, in minutes");
  purple_plugin_pref_set_bounds(ppref, 1, 0xFFFF);
  purple_plugin_pref_frame_add(frame, ppref);

  return frame;
}

static PurplePluginUiInfo prefs_info = {.get_plugin_pref_frame = get_plugin_pref_frame};

static PurplePluginInfo info = {
  .magic         = PURPLE_PLUGIN_MAGIC,
  .major_version = PURPLE_MAJOR_VERSION,
  .minor_version = PURPLE_MINOR_VERSION,
  .type          = PURPLE_PLUGIN_STANDARD,
  .priority      = PURPLE_PRIORITY_DEFAULT,
  .load          = plugin_load,
  .unload        = plugin_unload,
  .prefs_info    = &prefs_info,
  .id            = "core-libetpan_mail_check",
  .name          = PLUGIN_NAME,
  .version       = "0.5",
  .summary       = "New mail notifications",
  .description   =
    "This plugin provides new mail notifications.\n"
    "For this to work, you need to enable IMAP access to your account.\n"
};

PURPLE_INIT_PLUGIN(libetpan_mail_check, init_plugin, info);
