# SPDX-License-Identifier: 0BSD

CFLAGS?=-Os -Wall
ETPAN_CFLAGS!=libetpan-config --cflags --libs
PURPLE_CFLAGS!=pkg-config --cflags purple
PURPLE_PLUGINDIR!=pkg-config --variable=plugindir purple

all: libetpan_mail_check.so
.PHONY: all

libetpan_mail_check.so: libetpan_mail_check.c
	${CC} libetpan_mail_check.c ${PURPLE_CFLAGS} ${ETPAN_CFLAGS} ${CFLAGS} -fpic -shared -o libetpan_mail_check.so
clean:; rm -f libetpan_mail_check.so
.PHONY: clean

install: install_system
install_home  : libetpan_mail_check.so; install -Dt ${HOME}/.purple/plugins         libetpan_mail_check.so
install_system: libetpan_mail_check.so; install -Dt "${DESTDIR}${PURPLE_PLUGINDIR}" libetpan_mail_check.so
.PHONY: install install_home install_system

uninstall: uninstall_system
uninstall_home  :; rm -f ${HOME}/.purple/plugins/libetpan_mail_check.so
uninstall_system:; rm -f ${PURPLE_PLUGINDIR}/libetpan_mail_check.so
.PHONY: uninstall uninstall_home uninstall_system
